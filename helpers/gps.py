#!/usr/bin/python
# -*- coding: utf-8 -*-
from config.props import ADB_PATH, EMULATOR_NAME
# example data, should set Moscow to emulator
HOST = "127.0.0.1"
PORT = 5554
TIMEOUT = 10
LNG_SRC = 37.6134591
LAT_SRC = 55.7535942
LNG_DST = 38.6134591
LAT_DST = 55.9535942
SECONDS = 120


def mock_gps_emulator(lon, lat, emulator=EMULATOR_NAME, adb_bin=ADB_PATH):
    """
    Set long lat for gps in Android Emulator
    :param lon:
    :param lat:
    :param emulator:
    :param adb_bin:
    :return:
    """

    from subprocess import call

    cmd = r'"%s" -s %s emu geo fix %s %s' % (adb_bin, emulator, str(lon), str(lat))
    call(cmd, shell=True)


def mock_gps_real_device(lon, lat, device):
    """
    Mock GPS coords using Fake gps
    currently device is manually set with data in Fake GPS app
    automate Fake GPS to set coordinates as to mock GPS with own data programmatically

    :param lon:
    :param lat:
    :return:
    """
    pass

if __name__ == '__main__':
    mock_gps_emulator(LNG_SRC, LAT_SRC)
