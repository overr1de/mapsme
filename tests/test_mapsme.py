from views.base_mapsme_view import BaseMapsmeView
from views.map_selected_item_view import MapSelectedItemView


def test_mapsme(setup_func):
    driver = setup_func

    # TODO: dump cyanogen wileyfox activity for menu, excluded from the test due to rarity
    # NOTE: the GPS is faked manually by Fake GPS app

    initial_page = BaseMapsmeView(driver)
    search = initial_page.go_to_search()
    results = search.pick_category('Еда')
    assert results.number_of_results != 0
    map_selected_item = results.choose_search_result('Руккола')

    # Brief and detailed info
    map_selected_item.expand_info()
    map_selected_item.validate_expanded_info()
    map_selected_item.verify_coordinates_switch()
    map_selected_item.verify_coordinates_switch()

    workhours_before = map_selected_item.workhours  # saving for further assert

    # Editing functionality
    place_editor = map_selected_item.go_to_place_editor()
    place_editor.add_language('Latin')
    place_editor.new_language_is_added('International (Latin)')
    place_editor.set_value_for_field('International (Latin)', 'Rukkola Latin')
    place_editor.set_value_for_field('Почтовый индекс', '109012')
    place_editor.change_work_hours_to(start='9:30', end='21:17')
    place_editor.validate_time_is_set(open_expected='09:30', close_expected='21:17')
    place_editor.save()
    place_editor.set_value_for_field('Email', text='test@test.com')
    place_editor.go_to_cuisine()
    place_editor.pick('Австрийская кухня')
    place_editor.save()
    place_editor.toggle_wifi()
    place_editor.save()
    place_editor.react_to_alert(confirm=True)
    place_editor.validate_auth_is_required(to_close=True)

    # Updated detailed info
    new_map_selected_item = MapSelectedItemView(driver)
    assert workhours_before != new_map_selected_item.workhours
    assert new_map_selected_item.email
    assert new_map_selected_item.wifi
    assert new_map_selected_item.phone
    assert 'Австрийская кухня' in new_map_selected_item.cuisine


