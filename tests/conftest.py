import os
import pytest

from appium import webdriver

from helpers.gps import mock_gps_emulator
from config.props import REAL_DEVICE_WILLEYFOX, APK_PATH, MOSCOW_CENTER_LAT, MOSCOW_CENTER_LONG


def pytest_addoption(parser):
    parser.addoption("--mockgps", action="store", default="moscow_center",
                     help="Provide the lon, lat '37.6134591,55.7535942' ")
    parser.addoption("--device", action="store", default="emulator-5554",
                     help="Provide the device name listed in >adb devices for test")


@pytest.fixture(scope='function')
def setup_func(request):
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '6.0'
    desired_caps['deviceName'] = REAL_DEVICE_WILLEYFOX
    desired_caps['app'] = os.path.abspath(APK_PATH)
    # desired_caps['appPackage'] = '=com.cyanogenmod.trebuchet'
    # desired_caps['appActivity'] = 'com.android.launcher3.Launcher'
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
    mock_gps_emulator(MOSCOW_CENTER_LONG, MOSCOW_CENTER_LAT)

    def tearDown():
        driver.quit()
    request.addfinalizer(tearDown)
    return driver
