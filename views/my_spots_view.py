from views.base_appium_view import BaseAppiumView


class MySpotsView(BaseAppiumView):
    """
    View for user stored sights he added to favorites list
    """
    pass