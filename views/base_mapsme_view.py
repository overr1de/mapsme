from views.base_appium_view import BaseAppiumView


def view_factory(view, driver):
    if view not in ['main_menu', 'map_list', 'my_spots', 'search_view', 'search_result_view', 'map_selected_item_view',
                    'place_editor']:
        raise Exception("Please provide the existing and correct name for the view in the application")

    if view == 'main_menu':
        from views.main_menu import MainMenu
        return MainMenu(driver)
    elif view == 'map_list':
        from views.map_list import MapList
        return MapList(driver)
    elif view == 'my_spots':
        from views.my_spots_view import MySpotsView
        return MySpotsView(driver)
    elif view == 'search_view':
        from views.search_view import SearchView
        return SearchView(driver)
    elif view == 'search_result_view':
        from views.search_result_view import SearchResultView
        return SearchResultView(driver)
    elif view == 'map_selected_item_view':
        from views.map_selected_item_view import MapSelectedItemView
        return MapSelectedItemView(driver)
    elif view == 'place_editor':
        from views.place_editor_view import PlaceEditorView
        return PlaceEditorView(driver)


class BaseMapsmeView(BaseAppiumView):

    # selectors IDs
    MENU_BUTTON = 'com.mapswithme.maps.pro:id/menu'
    MINUS = 'com.mapswithme.maps.pro:id/nav_zoom_out'
    PLUS = 'com.mapswithme.maps.pro:id/nav_zoom_in'
    MY_POSITION = 'com.mapswithme.maps.pro:id/my_position'
    SEARCH = 'com.mapswithme.maps.pro:id/search'
    P2P = 'com.mapswithme.maps.pro:id/p2p'
    BOOKMARKS = 'com.mapswithme.maps.pro:id/bookmarks'

    def __init__(self, driver):
        super().__init__(driver)
        self._validate_gps_is_resolved()

    def go_to_menu(self):
        """
        Click at main menu button
        :return:
        """
        self.get_element(self.MENU_BUTTON).click()
        return view_factory('main_menu', self.driver)

    def go_to_search(self):
        """
        Enter search screen
        :return:
        """
        self.get_element(self.SEARCH).click()
        return view_factory('search_view', self.driver)

    def zoom_in(self):
        """
        Zoom in for a detailed view on the map
        :return:
        """
        self.get_element(self.PLUS).click()

    def zoom_out(self):
        """
        Zoom out for a more commob view on the map
        :return:
        """
        self.get_element(self.MINUS).click()

    def locate_my_coordinates(self):
        """
        click at My Position button to center the map at where GPS set user's coordinates
        :return:
        """

        # TODO add after\before verification of a changed map state
        # before = self._get_map_outline()
        self.get_element(self.MY_POSITION).click()
        # after = self._get_map_outline()
        # assert before != after

    def _get_map_outline(self):
        """
        Future method to get screenshot and calculate it in a approximate way represented in binary data
        Should be used to see slight changes on the map
        :return:
        """
        pass

    def _validate_gps_is_resolved(self):
        """
        Get the state of the button and the GPS coords
        :return:
        """