from views.base_appium_view import BaseAppiumView
from views.base_mapsme_view import view_factory


class MapSelectedItemView(BaseAppiumView):
    # brief info
    TITLE = "com.mapswithme.maps.pro:id/tv__title"
    SUBTITLE = "com.mapswithme.maps.pro:id/tv__subtitle"
    STRAIGHT_DISTANCE = "com.mapswithme.maps.pro:id/tv__straight_distance"
    INFO_PANEL = "com.mapswithme.maps.pro:id/pp__details"
    ADDRESS = "com.mapswithme.maps.pro:id/tv__address"
    ARROW = "com.mapswithme.maps.pro:id/av__direction"
    PREVIEW = "com.mapswithme.maps.pro:id/pp__preview"
    # expanded info
    WORKHOURS = "com.mapswithme.maps.pro:id/today_opening_hours"
    WEBSITE = "com.mapswithme.maps.pro:id/tv__place_website"
    PHONE_NUMBER = "com.mapswithme.maps.pro:id/tv__place_phone"
    LATLON = "com.mapswithme.maps.pro:id/tv__place_latlon"
    CUISINE = "com.mapswithme.maps.pro:id/tv__place_cuisine"
    EDIT_INFO = "com.mapswithme.maps.pro:id/ll__place_editor"
    EMAIL = "com.mapswithme.maps.pro:id/tv__place_email"
    WIFI = "com.mapswithme.maps.pro:id/tv__place_wifi"

    def __init__(self, driver):
        super().__init__(driver)
        self._validate_basic_elements_are_present()

    def _validate_basic_elements_are_present(self):
        self.get_element(self.INFO_PANEL)
        self.get_element(self.TITLE)
        self.get_element(self.SUBTITLE)
        self.get_element(self.STRAIGHT_DISTANCE)
        self.get_element(self.ADDRESS)
        self.get_element(self.ARROW)

    def expand_info(self):
        self.get_element(self.PREVIEW).click()

    def validate_expanded_info(self):
        self.get_element(self.WORKHOURS)
        self.get_element(self.WEBSITE)
        self.get_element(self.PHONE_NUMBER)
        self.get_element(self.LATLON)
        self.get_element(self.CUISINE)
        self.get_element(self.EDIT_INFO)

    def verify_coordinates_switch(self):

        coordinates = self.get_element("com.mapswithme.maps.pro:id/ll__place_latlon")

        before_text = self.get_element(self.LATLON).text
        coordinates.click()
        after_text = self.get_element(self.LATLON).text
        assert before_text != after_text

    def go_to_place_editor(self):

        self.get_element(self.EDIT_INFO).click()
        return view_factory('place_editor', self.driver)

    @property
    def workhours(self):
        return self.get_element(self.WORKHOURS).text

    @property
    def email(self):
        return self.get_element(self.EMAIL).text

    @property
    def wifi(self):
        return self.get_element(self.WIFI).text

    @property
    def phone(self):
        return self.get_element(self.PHONE_NUMBER).text

    @property
    def cuisine(self):
        return self.get_element(self.CUISINE).text