from views.base_appium_view import BaseAppiumView
from views.base_mapsme_view import view_factory


class SearchResultView(BaseAppiumView):
    """
    View with the results of the search query or category contents
    """
    ITEM_TITLE = "com.mapswithme.maps.pro:id/title"
    ITEM_CARD = "//android.support.v7.widget.RecyclerView//android.widget.RelativeLayout"

    def choose_search_result(self, text):
        if isinstance(text, int):
            self.get_elements(self.ITEM_TITLE)[text].click()
            return view_factory('map_selected_item_view', self.driver)
        else:
            wanted_idx = None
            for idx, item in enumerate(self.get_elements(self.ITEM_TITLE)):
                if text == item.text:
                    wanted_idx = idx
                    break
            self.get_elements(self.ITEM_CARD, by_xpath=True)[wanted_idx].click()
            return view_factory('map_selected_item_view', self.driver)

    @property
    def number_of_results(self):
        return len(self.get_elements(self.ITEM_TITLE))
