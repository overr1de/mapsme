from views.base_appium_view import BaseAppiumView
from views.base_mapsme_view import view_factory
import time


class SearchView(BaseAppiumView):
    """
    View to provide user with search actions
    """
    SEARCH_FIELD = "com.mapswithme.maps.pro:id/query"
    TABS = r"//android.widget.LinearLayout//android.support.v7.app.ActionBar$Tab[@index='1']"
    CATEGORIES = "//android.widget.TextView[@text='%s']"

    def search_for(self, text):
        self.get_element(self.SEARCH_FIELD).send_keys(text)
        return view_factory('search_result_view', self.driver)

    def pick_category(self, text):
        self.get_element("//*[contains(@text, 'Категории')]", by_xpath=True).click()
        time.sleep(3)  # TODO: refactor
        self.get_element(r"//android.support.v7.widget.RecyclerView//android.widget.TextView[contains(@text, '%s')]" % text, by_xpath=True).click()
        return view_factory('search_result_view', self.driver)

