import time
from config.props import TIMEOUT


class BaseAppiumView(object):
    """
    BaseAppiumPage is a PageObject class to implement common methods to obtain elements of the application
    or make actions that are common throughout all the applications
    """
    def __init__(self, driver):
        self.driver = driver

    def get_element(self, selector, by_id=True, by_xpath=False):
        """
        Get element of the page with the given selector
        :return:
        """
        time.sleep(1)  # initial timeout for minimal view render
        self.driver.implicitly_wait(TIMEOUT)
        if by_xpath:
            return self.driver.find_element_by_xpath(selector)
        elif by_id:
            return self.driver.find_element_by_id(selector)

    def get_elements(self, selector, by_id=True, by_xpath=False):
        """
        Get elements with the same selector
        :param selector:
        :return:
        """
        self.driver.implicitly_wait(TIMEOUT)

        if by_xpath:
            return self.driver.find_elements_by_xpath(selector)
        elif by_id:
            return self.driver.find_elements_by_id(selector)

    def scroll(self, move='down'):
        """
        Scrolls down or up based in accordance with screen size
        :param move:
        :return:
        """
        size = self.driver.get_window_size()
        top_y = int(size["height"] * 0.20)
        bottom_y = int(size["height"] * 0.80)

        startx = int(size["width"] / 2)
        if move == 'up':
            self.driver.swipe(startx, top_y, startx, bottom_y, 3000)
        elif move == 'down':
            self.driver.swipe(startx, bottom_y, startx, top_y, 3000)
