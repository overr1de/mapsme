import time
from selenium.common.exceptions import NoSuchElementException

from views.base_appium_view import BaseAppiumView


class PlaceEditorView(BaseAppiumView):
    WORKHOURS = "com.mapswithme.maps.pro:id/opening_hours"
    PHONE_NUMBER = "com.mapswithme.maps.pro:id/input"  # NOTE: should we rename it, too broad term 'input'
    CUISINE = "com.mapswithme.maps.pro:id/block_cuisine"
    CUISINE_ITEM = "com.mapswithme.maps.pro:id/cuisine"
    WEBSITE = "com.mapswithme.maps.pro:id/input"
    ADD_LANG = "com.mapswithme.maps.pro:id/add_langs"
    LANG_ITEM = "com.mapswithme.maps.pro:id/name"
    WIFI = "com.mapswithme.maps.pro:id/sw__wifi"
    SAVE = "com.mapswithme.maps.pro:id/save"

    def __init__(self, driver):
        super().__init__(driver)
        self.validate_basic_elements_are_present()

    def validate_basic_elements_are_present(self):
        """
        static verification for elements' presence
        :return:
        """
        self.scroll(move='down')
        self.get_element(self.WORKHOURS)
        self.scroll(move='down')
        self.get_element(self.CUISINE)
        found_elements_text = [element.text for element in
                                                   self.get_elements("com.mapswithme.maps.pro:id/custom_input")]
        print(found_elements_text[:3])
        assert ['Телефон', 'Вебсайт', 'Email'] == found_elements_text[:3]
        self.scroll(move='up')
        self.scroll(move='up')

    def add_language(self, language_text):
        """
        Scroll through language list to pick the chosen one
        :param language_text:
        :return:
        """
        self.get_element(self.ADD_LANG).click()

        #  Scrolling down
        for r in range(8):
            self.scroll('down')
            languages = self.get_elements(self.LANG_ITEM)
            for l in languages:
                if language_text in l.text:
                    l.click()
                    return

    def new_language_is_added(self, language):
        """
        Verify language placeholder is visible after the language has been added
        :param language:
        :return:
        """
        self.get_element("//TextInputLayout[@text='%s']" % language, by_xpath=True)

    def set_value_for_field(self, field, text):
        """
        Basic method to enter information in EditText
        :param field:
        :param text:
        :return:
        """
        for r in range(10):
            try:
                input = self.get_element("//TextInputLayout[@text='%s']//android.widget.EditText" % field, by_xpath=True)
                input.click()
                input.send_keys(text)
                self.driver.back()
                break
            except NoSuchElementException:
                self.scroll('down')

    def change_work_hours_to(self, start, end):
        """
        Pass time in '9:14' format to set it
        :param start:
        :param end:
        :return:
        """
        # parse the input time
        from_hours, from_minutes = [item.strip() for item in start.split(":")]
        to_hours, to_minutes = [item.strip() for item in end.split(":")]

        # click to enter time form
        for r in range(15):
            try:
                edit_work_hours = self.get_element(self.WORKHOURS)
                break
            except NoSuchElementException:
                self.scroll('down')
        edit_work_hours.click()

        def change_hours(hour):
            # TODO: time widget elements have innerclass and dollar sign, exception is raised
            # Accessing child nodes from view is not working
            hours_items = self.get_elements("//android.view.View/child::node()", by_xpath=True)
            print(len(hours_items))
            hours_items[int(hour)].click()

        def change_minutes(minutes):
            minutes_index = (int(minutes) // 5)
            # TODO: add logic for quarters and decision making about swipe direction
            minus_items = self.get_elements("//android.view.View/child::node()", by_xpath=True)
            minus_items[minutes_index].click()

            def calibrate_time(element, wanted_time):
                if int(wanted_time) % 5 == 0:
                    return  # no need to calibrate time
                loc = element.location
                x = loc['x']
                y = loc['y']
                for r in range(20):
                    self.driver.swipe(x, y, x, y + 5, 300)
                    y = int(y) + 5
                    if self.get_element("android:id/minutes").text == wanted_time:
                        return

            calibrate_time(minus_items[minutes_index], wanted_time=minutes)

        self.get_element("com.mapswithme.maps.pro:id/time_open").click()
        change_hours(from_hours)
        change_minutes(from_minutes)

        time.sleep(1)
        self.get_element("android:id/button1").click()
        change_hours(to_hours)
        change_minutes(to_minutes)
        time.sleep(1)
        self.get_element("android:id/button1").click()

    def validate_time_is_set(self, open_expected, close_expected):
        """

        :param open_expected:
        :param close_expected:
        :return:
        """
        open_actual = self.get_element("com.mapswithme.maps.pro:id/tv__time_open").text
        close_actual = self.get_element("com.mapswithme.maps.pro:id/tv__time_close").text

        assert close_actual == close_expected
        assert open_actual == open_expected

    def save(self):
        """
        Submit changes
        :return:
        """
        self.get_element(self.SAVE).click()

    def go_to_cuisine(self):
        """
        Enter cuisine list
        :return:
        """
        for r in range(5):
            try:
                self.get_element(self.CUISINE).click()
                break
            except NoSuchElementException:
                self.scroll('down')

    def pick(self, text):
        """
        Pick new cuisine type from the list by providing text
        :param text:
        :return:
        """
        items = self.get_elements(self.CUISINE_ITEM)
        index = None
        for idx, item in enumerate(items):
            if text == item.text:
                index = idx
                break
        self.get_elements("//android.support.v7.widget.RecyclerView/android.widget.LinearLayout", by_xpath=True)[index].click()

    def toggle_wifi(self):
        """
        Switch on or off the wifi availability tumbler
        :return:
        """
        for r in range(5):
            try:
                self.get_element(self.WIFI).click()
                return
            except NoSuchElementException:
                self.scroll('down')

    def validate_auth_is_required(self, to_close=True):
        """
        verify that guest session offers to login after added info to firm card
        :param to_close:
        :return:
        """
        auth = self.get_element("com.mapswithme.maps.pro:id/block_auth")
        # action = TouchAction(self.driver)
        if to_close:
            self.driver.back()

    def react_to_alert(self, confirm=False):
        """
        If alert is present, react to it
        :param confirm:
        :return:
        """
        try:
            if self.get_element("com.mapswithme.maps.pro:id/alertTitle").text.startswith("Отправить всем"):
                if confirm:
                    self.get_element("android:id/button1").click()
                else:
                    self.get_element("android:id/button2").click()
        except NoSuchElementException:
            pass