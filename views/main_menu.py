from views.base_appium_view import BaseAppiumView
from views.base_mapsme_view import view_factory


class MainMenu(BaseAppiumView):
    SHARE_GPS = ""
    SETTINGS = ""
    LOAD_NEW_MAP = ""
    MY_SPOTS = ""
    SEARCH = 'com.mapswithme.maps.pro:id/btn__search'

    def select(self, text):
        # TODO: store the localization default choice (eng) and preload class with the corresponding texts from locale
        if text == 'Поделиться местоположением':
            self.get_element(self.SHARE_GPS).click()
        elif text == 'Настройки':
            self.get_element(self.SETTINGS).click()
        elif text == 'Загрузить карты':
            self.get_element(self.LOAD_NEW_MAP).click()
            return view_factory('map_list', self.driver)
        elif text == 'Метки':
            self.get_element(self.MY_SPOTS).click()
            return view_factory('my_spots', self.driver)
        elif text == 'Поиск':
            self.get_element(self.SEARCH).click()
            return view_factory('search_view', self.driver)
