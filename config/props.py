ANDROID_HOME = ""
ADB_PATH = r"C:\Program Files (x86)\Android\android-sdk\platform-tools\adb.exe"  # custom adb path

APK_PATH = r"C:\Users\override\PycharmProjects\mapsme\apps\android-web-universal-release-6.5.2-161105.apk"

EMULATOR_NAME = 'emulator-5554'  # default emulator, may be overriden here just in case
REAL_DEVICE_WILLEYFOX = '98b0e447'

MOSCOW_CENTER_LONG = 37.621184
MOSCOW_CENTER_LAT = 55.75674

TIMEOUT = 15